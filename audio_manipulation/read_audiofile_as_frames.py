import numpy as np
import soundfile as sf

from pathlib import Path
from typing import Optional, Any, List, Tuple


def sample_to_ms(len_samples: int, sr: int) -> int:
    return int((len_samples / sr) * 1000)


def ms_to_sample(ms: int, sr: int) -> int:
    return int((ms / 1000) * sr)


def get_frame_validity(frame_zero: np.ndarray) -> bool:
    # Expand this function when suited
    if not frame_zero.any():
        # Frame is filled with zeros.
        return False
    else:
        return True


def read_audiofile_as_frames(audio_path: Path, frame_length_ms: float) -> Tuple[List[np.ndarray], int]:
    samples, samplerate = sf.read(audio_path, dtype='int16')
    samples = samples.astype('float')

    frame_length_samples = ms_to_sample(int(frame_length_ms), samplerate)
    zeros_to_add_in_audio = frame_length_samples - (len(samples) % frame_length_samples)
    samples = np.append(samples, np.zeros(zeros_to_add_in_audio))
    number_of_frames = int(len(samples) / frame_length_samples)
    frames = samples.reshape(number_of_frames, frame_length_samples)

    return frames, samplerate


def read_signal_as_frames(signal: np.ndarray, frame_length_ms: float, samplerate: int) -> List[np.ndarray]:
    frame_length_samples = ms_to_sample(int(frame_length_ms), samplerate)
    last_frame_missing_samples = len(signal) % frame_length_samples
    zeros_to_add_in_audio = (frame_length_samples - last_frame_missing_samples) if last_frame_missing_samples > 0 else 0
    signal = np.append(signal, np.zeros(zeros_to_add_in_audio))
    number_of_frames = int(len(signal) / frame_length_samples)
    frames = signal.reshape(number_of_frames, frame_length_samples)

    return frames
