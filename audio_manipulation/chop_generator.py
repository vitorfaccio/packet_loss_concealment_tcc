import os

from read_audiofile_as_frames import *

REFERENCE_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'reference_16khz'
CHOPPED_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'chop_20ms_160ms_16khz'

frame_length_ms = 20  # 10, 20 or 40ms
distance_between_chopped_frames_ms = 160  # 80 or 160ms
if distance_between_chopped_frames_ms % frame_length_ms:
    raise RuntimeError("Distance between chopped frames must be a multiple of frame length.")

samplerate = 16000
frame_length_samples = ms_to_sample(frame_length_ms, samplerate)
distance_between_chopped_frames_samples = ms_to_sample(distance_between_chopped_frames_ms, samplerate)
distance_between_chopped_frames_in_frames = int(distance_between_chopped_frames_samples / frame_length_samples)


try:
    all_reference_audio_paths = sorted(REFERENCE_SET_DIR.glob('*.wav'))
    for reference_audio_path in all_reference_audio_paths:
        frames, sr_fromfile = read_audiofile_as_frames(reference_audio_path, frame_length_ms)
        if sr_fromfile != samplerate:
            raise RuntimeError("Samplerate in file {} is wrong, identified value: {}".format(reference_audio_path.name, sr_fromfile))

        chopped_frame_indexes = [i for i in range(len(frames)) if i % distance_between_chopped_frames_in_frames == 0]

        for index in chopped_frame_indexes:
            frames[index] = np.zeros(frame_length_samples)

        plain_samples_list = np.reshape(frames, (1, frame_length_samples*len(frames)))[0]
        new_file_name = list(reference_audio_path.name)
        new_file_name[0] = 'C'
        new_file_name = ''.join(new_file_name)
        plain_samples_list = plain_samples_list.astype('int16')
        sf.write(CHOPPED_SET_DIR / new_file_name, plain_samples_list, samplerate)

except Exception as e:
    print("Error occurred: {}".format(e))
