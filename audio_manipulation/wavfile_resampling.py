import os
import subprocess

from pathlib import Path

CHOPPY_SET_ORIGINAL_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_original' / 'chop'
REFERENCE_SET_ORIGINAL_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_original' / 'reference'

CHOPPY_SET_RESAMPLED_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_resampled' / 'chop'
REFERENCE_SET_RESAMPLED_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_resampled' / 'reference'

resampling_frequency = '16000'

try:
    choppy_audios_filelist = [f.path for f in os.scandir(CHOPPY_SET_ORIGINAL_DIR) if f.is_file()]
    reference_audios_filelist = [f.path for f in os.scandir(REFERENCE_SET_ORIGINAL_DIR) if f.is_file()]

    for i in range(len(choppy_audios_filelist)):
        dest_file = str(CHOPPY_SET_RESAMPLED_DIR) + "/" + choppy_audios_filelist[i][len(choppy_audios_filelist[i]) - 16:len(choppy_audios_filelist[i])]
        print(dest_file)
        process = subprocess.run(['sox', choppy_audios_filelist[i], '-r', resampling_frequency, dest_file],
                                 stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        if process.returncode != 0:
            print("Falha durante resampling com sox.")
            raise Exception("Falha durante resampling com sox.")

    for i in range(len(reference_audios_filelist)):
        dest_file = str(REFERENCE_SET_RESAMPLED_DIR) + "/" + reference_audios_filelist[i][len(reference_audios_filelist[i]) - 16:len(reference_audios_filelist[i])]
        print(dest_file)
        process = subprocess.run(['sox', reference_audios_filelist[i], '-r', resampling_frequency, dest_file],
                                 stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        if process.returncode != 0:
            print("Falha durante resampling com sox.")
            raise Exception("Falha durante resampling com sox.")

except Exception as e:
    print("Error occurred: {}".format(e))






