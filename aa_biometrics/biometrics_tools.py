import os
import requests
import logging

from typing import Optional
from pathlib import Path

API_URL = os.getenv('API_URL')
API_KEY = os.getenv('API_KEY')
HEADER = {'khomp-api-key': API_KEY}


def create_profile() -> str:
    # Conteúdo oculto
    return profile_id


def create_audio(profile_id: str, recording_path: Path) -> Optional[str]:
    # conteúdo oculto
    return None


def start_training(profile_id: str) -> Optional[str]:
    # Conteúdo oculto
    return None


def verify_audio(profile_id: str, recording_path: Path) -> float:
    # Conteúdo oculto
    return confidence
