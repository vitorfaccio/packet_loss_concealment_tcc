import csv

from datetime import datetime
from logs.logger import *
from biometrics_tools import *

REFERENCE_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'reference_16khz'
CHOPPED_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'chop_20ms_160ms_16khz'
CORRECTED_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'corrected_lpc_bilateral_v1'  # Change here!
TEST_SET_DIR = CORRECTED_SET_DIR  # Change here!

execution_date = datetime.now().strftime("%Y%m%d%H%M%S")
LOG_DIR = Path(os.getcwd()).parent.resolve() / 'logs'
logger = configure_logger(LOG_DIR, execution_date)

logger.info("Iniciando teste de biometria no diretório {}.".format(TEST_SET_DIR))
print("Iniciando teste de biometria no diretório {}.".format(TEST_SET_DIR))


try:
    reference_audios_filelist = [f.path for f in os.scandir(REFERENCE_SET_DIR) if f.is_file()]
    tested_audios_filelist = [f.path for f in os.scandir(TEST_SET_DIR) if f.is_file()]

    reference_audios_filelist_per_speaker = {
        'FA': [f for f in sorted(reference_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'FA')],
        'FG': [f for f in sorted(reference_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'FG')],
        'MK': [f for f in sorted(reference_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'MK')],
        'ML': [f for f in sorted(reference_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'ML')],
    }

    tested_audios_filelist_per_speaker = {
        'FA': [f for f in sorted(tested_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'FA')],
        'FG': [f for f in sorted(tested_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'FG')],
        'MK': [f for f in sorted(tested_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'MK')],
        'ML': [f for f in sorted(tested_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'ML')],
    }

    complete_results = {
        'FA': {'FA': [], 'FG': [], 'MK': [], 'ML': []},
        'FG': {'FA': [], 'FG': [], 'MK': [], 'ML': []},
        'MK': {'FA': [], 'FG': [], 'MK': [], 'ML': []},
        'ML': {'FA': [], 'FG': [], 'MK': [], 'ML': []},
    }

    for speaker in reference_audios_filelist_per_speaker.keys():
        # Criar profile para o locutor X
        logger.info("Criando profile para locutor {}.".format(speaker))
        print("Criando profile para locutor {}.".format(speaker))
        profile_id = create_profile()

        # Enviar áudios de treinamento para o locutor X
        logger.info("Enviando áudios de treinamento para locutor {}.".format(speaker))
        print("Enviando áudios de treinamento para locutor {}.".format(speaker))
        # TODO: Randomize indexes of picked audios for profile training, maybe?
        for wav_name in reference_audios_filelist_per_speaker[speaker][:3]:
            result_create_audio = create_audio(profile_id, Path(wav_name))
            logger.info("Áudio criado para locutor {}: {}".format(speaker, wav_name))
            print("Áudio criado para locutor {}: {}".format(speaker, wav_name))

        # Fazer treinamento para o locutor X
        logger.info("Iniciando treinamento para locutor {}.".format(speaker))
        print("Iniciando treinamento para locutor {}.".format(speaker))
        result_start_training = start_training(profile_id)

        # Verificar áudios com todos os locutores (usuários)
        for tester in tested_audios_filelist_per_speaker.keys():
            logger.info("Iniciando verificação de áudios com locutor {}.".format(tester))
            print("Iniciando verificação de áudios com locutor {}.".format(tester))
            for wav_name in tested_audios_filelist_per_speaker[tester][3:]:

                single_audio_average = 0
                for k in range(3):
                    single_result_verify = verify_audio(profile_id, Path(wav_name))
                    single_audio_average += single_result_verify
                result_verify = single_audio_average / 3
                complete_results[speaker][tester].append(result_verify)

                # result_verify = verify_audio(profile_id, Path(wav_name))
                # complete_results[speaker][tester].append(result_verify)
                logger.info("Áudio verificado para locutor original {} e testador {}: {}. Resultado: {}".format(speaker, tester, wav_name, result_verify))
                print("Áudio verificado para locutor original {} e testador {}: {}. Resultado: {}".format(speaker, tester, wav_name, result_verify))
            average = [value for value in complete_results[speaker][tester] if value >= 0.]
            average = sum(average)/len(average)
            complete_results[speaker][tester].append(average)


    # Salvar em .CSV os resultados disponíveis no complete_results
    with open(LOG_DIR / 'temporary_results' / "biometrics_{}_{}.csv".format(TEST_SET_DIR.name, execution_date), "w", newline="") as file:
        header = ['Locutor original', 'Perfil testado',
                  '04',
                  '05',
                  '06',
                  '07',
                  '08',
                  '09',
                  '10',
                  '11',
                  '12',
                  '13',
                  '14',
                  '15',
                  '16',
                  '17',
                  '18',
                  '19',
                  '20',
                  '21',
                  '22',
                  '23',
                  '24',
                  'Média',
                  ]
        csvwriter = csv.writer(file)
        csvwriter.writerow(["Test set: {}".format(TEST_SET_DIR.name)])
        csvwriter.writerow(header)

        for original_speaker, value_upper in complete_results.items():
            for tester, value_lower in value_upper.items():
                data = [original_speaker] + [tester] + value_lower
                csvwriter.writerow(data)

except Exception as e:
    print("Error occurred: {}".format(e))
    logger.error("Error occurred: {}".format(e))
