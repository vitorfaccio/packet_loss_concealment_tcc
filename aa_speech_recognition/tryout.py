import os
import speech_recognition as sr

from jiwer import wer, cer
from pathlib import Path

# https://github.com/Uberi/speech_recognition/blob/master/examples/audio_transcribe.py
# Dataset is expected to have a pair of file lists with the same amount of
# files, all recorded with a single sample rate. "Dataset" project does this checking.

ref_audio_path = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'reference_16khz' / 'R_01_CHOP_FA.wav'
chop_audio_path = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'chop_20ms_160ms_16khz' / 'C_01_CHOP_FA.wav'
corr_g711_v1_audio_path = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'corrected_g711_v1' / 'CORRECTED_C_01_CHOP_FA.wav'
corr_lpc_bilateral_audio_path = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'corrected_lpc_bilateral_v1' / 'CORRECTED_C_01_CHOP_FA.wav'

REFERENCE_RESULT_FILE = Path(os.getcwd()).resolve() / "result_reference.txt"


r = sr.Recognizer()

with sr.AudioFile(str(ref_audio_path)) as source_ref, \
        sr.AudioFile(str(chop_audio_path)) as source_chop, \
        sr.AudioFile(str(corr_g711_v1_audio_path)) as source_corr_g711_v1, \
        sr.AudioFile(str(corr_lpc_bilateral_audio_path)) as source_corr_lpc_bilateral:
    audio_ref = r.record(source_ref)  # read the entire audio file
    audio_chop = r.record(source_chop)  # read the entire audio file
    audio_corr_g711 = r.record(source_corr_g711_v1)  # read the entire audio file
    audio_corr_lpc_bilateral = r.record(source_corr_lpc_bilateral)  # read the entire audio file

    result_ref = r.recognize_sphinx(audio_ref)
    print("Reference: {}".format(result_ref))
    result_chop = r.recognize_sphinx(audio_chop)
    print("Chopped: {}".format(result_chop))
    result_corr_g711 = r.recognize_sphinx(audio_corr_g711)
    print("Corrected G711: {}".format(result_corr_g711))
    result_corr_lpc_bilateral = r.recognize_sphinx(audio_corr_lpc_bilateral)
    print("Corrected LPC bilateral: {}".format(result_corr_lpc_bilateral))

    wer_value_chop = wer(result_ref, result_chop)
    cer_value_chop = cer(result_ref, result_chop)

    wer_value_corr_g711 = wer(result_ref, result_corr_g711)
    cer_value_corr_g711 = cer(result_ref, result_corr_g711)

    wer_value_corr_lpc_bilateral = wer(result_ref, result_corr_lpc_bilateral)
    cer_value_corr_lpc_bilateral = cer(result_ref, result_corr_lpc_bilateral)

    print()
    print("Chopped ----------------- WER = {} ; CER = {}".format(wer_value_chop, cer_value_chop))
    print("Corrected G711 ---------- WER = {} ; CER = {}".format(wer_value_corr_g711, cer_value_corr_g711))
    print("Corrected lpc_bilateral - WER = {} ; CER = {}".format(wer_value_corr_lpc_bilateral, cer_value_corr_lpc_bilateral))
