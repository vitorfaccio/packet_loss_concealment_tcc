import speech_recognition
import speech_recognition as sr

from jiwer import wer, cer
from pathlib import Path


def recognize_audio_content(audio_path: str, r: speech_recognition.Recognizer) -> str:
    with sr.AudioFile(audio_path) as audio_file:
        audio_signal = r.record(audio_file)
        audio_content = r.recognize_sphinx(audio_signal)

    return audio_content
