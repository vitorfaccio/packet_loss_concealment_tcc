import os
import csv

from datetime import datetime
from logs.logger import *

from speech_recognition_tools import *

REFERENCE_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'reference_16khz'
CHOPPED_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'chop_20ms_160ms_16khz'
CORRECTED_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'corrected_lpc_bilateral_v1'
TEST_SET_DIR = CORRECTED_SET_DIR  # Change here!

execution_date = datetime.now().strftime("%Y%m%d%H%M%S")
LOG_DIR = Path(os.getcwd()).parent.resolve() / 'logs'
logger = configure_logger(LOG_DIR, execution_date)

logger.info("Iniciando teste de speech recognition no diretório {}.".format(TEST_SET_DIR))
print("Iniciando teste de speech recognition no diretório {}.".format(TEST_SET_DIR))

r = sr.Recognizer()

try:
    reference_audios_filelist = sorted([f.path for f in os.scandir(REFERENCE_SET_DIR) if f.is_file()])
    tested_audios_filelist = sorted([f.path for f in os.scandir(TEST_SET_DIR) if f.is_file()])

    reference_audios_filelist_per_speaker = {
        'FA': [f for f in sorted(reference_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'FA')],
        'FG': [f for f in sorted(reference_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'FG')],
        'MK': [f for f in sorted(reference_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'MK')],
        'ML': [f for f in sorted(reference_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'ML')],
    }

    tested_audios_filelist_per_speaker = {
        'FA': [f for f in sorted(tested_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'FA')],
        'FG': [f for f in sorted(tested_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'FG')],
        'MK': [f for f in sorted(tested_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'MK')],
        'ML': [f for f in sorted(tested_audios_filelist) if (f[len(f) - 6:len(f) - 4] == 'ML')],
    }

    complete_results = {
        'FA': {'reference': [], 'hypothesis': [], 'wer': [], 'cer': []},
        'FG': {'reference': [], 'hypothesis': [], 'wer': [], 'cer': []},
        'MK': {'reference': [], 'hypothesis': [], 'wer': [], 'cer': []},
        'ML': {'reference': [], 'hypothesis': [], 'wer': [], 'cer': []},
    }

    for speaker in reference_audios_filelist_per_speaker.keys():
        for i in range(len(reference_audios_filelist_per_speaker[speaker])):
            wav_name_ref = reference_audios_filelist_per_speaker[speaker][i]
            wav_name_hyp = tested_audios_filelist_per_speaker[speaker][i]

            audio_content_ref = recognize_audio_content(wav_name_ref, r)
            complete_results[speaker]['reference'].append(audio_content_ref)

            audio_content_hyp = recognize_audio_content(wav_name_hyp, r)
            complete_results[speaker]['hypothesis'].append(audio_content_hyp)

            wer_value = wer(audio_content_ref, audio_content_hyp)
            cer_value = cer(audio_content_ref, audio_content_hyp)
            complete_results[speaker]['wer'].append(wer_value)
            complete_results[speaker]['cer'].append(cer_value)

        average_wer = [value for value in complete_results[speaker]['wer']]
        average_wer = sum(average_wer) / len(average_wer)
        complete_results[speaker]['wer'].append(average_wer)

        average_cer = [value for value in complete_results[speaker]['cer']]
        average_cer = sum(average_cer) / len(average_cer)
        complete_results[speaker]['cer'].append(average_cer)

    # for speaker in complete_results.keys():
    #     data_aa = [complete_results[speaker]['cer']]
    #     data = [speaker] + \
    #            data_aa
    #     print(data)

    # Full CSV with transcribed phrases
    with open(LOG_DIR / 'temporary_results' / "speech_recog_{}_{}.csv".format(TEST_SET_DIR.name, execution_date), "w", newline="") as file:
        header = ['Speaker',
                  'Number',
                  'WER',
                  'CER',
                  'Reference string',
                  'Hypothesis string',
                  ]
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)

        for speaker in complete_results.keys():
            for i in range(len(reference_audios_filelist_per_speaker['FA'])):
                data = [speaker] + \
                       [str(i+1)] + \
                       [complete_results[speaker]['wer'][i]] + \
                       [complete_results[speaker]['cer'][i]] + \
                       [complete_results[speaker]['reference'][i]] + \
                       [complete_results[speaker]['hypothesis'][i]]
                csvwriter.writerow(data)

    # CER values CSV
    with open(LOG_DIR / 'temporary_results' / "speech_recog_ONLY_CER_{}_{}.csv".format(TEST_SET_DIR.name, execution_date), "w", newline="") as file:
        header = ['Audio file',
                  '01',
                  '02',
                  '03',
                  '04',
                  '05',
                  '06',
                  '07',
                  '08',
                  '09',
                  '10',
                  '11',
                  '12',
                  '13',
                  '14',
                  '15',
                  '16',
                  '17',
                  '18',
                  '19',
                  '20',
                  '21',
                  '22',
                  '23',
                  '24',
                  'Media',
                  ]
        csvwriter = csv.writer(file)
        csvwriter.writerow(header)

        for speaker in complete_results.keys():
            data = [speaker] + \
                   [complete_results[speaker]['cer']][0]
            csvwriter.writerow(data)

except Exception as e:
    print("Error occurred: {}".format(e))
    logger.error("Error occurred: {}".format(e))
