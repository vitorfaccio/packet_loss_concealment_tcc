import numpy as np
import scipy.signal

from typing import Tuple


# Implementação de função de detecção de pitch como descrita por Gunduzhan e Momtahan (2001)
def pitch_detector(e_n: np.ndarray, p_min: int, p_max: int, C: int) -> Tuple[int, int]:
    # p_min and p_max: minimum and maximum pitch periods in samples
    # C: Correlation size

    max_pitch = 0
    max_pitch_index = 0
    for i in range(p_min, p_max+1):                     # for i in [40:239]
        sum_num = 0.
        sum_den = 0.
        for n in range(len(e_n)-C, len(e_n)):           # for n in [120:320]
            index = n-i
            if index < 0: continue
            sum_num = sum_num + (e_n[n-i] * e_n[n])
            sum_den = sum_den + (e_n[n-i] * e_n[n-i])
        pitch = sum_num / np.sqrt(sum_den) if sum_den != 0. else 0.

        if pitch > max_pitch:
            max_pitch = pitch
            max_pitch_index = i

    # TODO: Gunduzhan, p.781: "Using a multiple of the pitch period ( 10 ms) for signals with
    #   low autocorrelation, we do not introduce artificial frequencies
    #   that could be caused by repeating a small segment many times.
    #   On the other hand, if the calculated autocorrelation is low but
    #   the previous speech happens to be voiced, we still maintain periodicity and generate a smooth excitation."
    if max_pitch_index < p_min:
        max_pitch_index = p_min
    return max_pitch_index, max_pitch


def pitch_waveform_replication_with_ola(frame_minus_one: np.ndarray, frame_minus_two: np.ndarray, frame_plus_one: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray, int]:
    analysed_signal = np.append(frame_minus_two[-120:], frame_minus_one)

    # Pitch detection
    pitch_max = 240  # 66 Hz
    pitch_min = 40   # 400 Hz
    pitch_correlation_size = 200
    pitch_period, pitch_value = pitch_detector(analysed_signal, pitch_min, pitch_max, pitch_correlation_size)
    print("Pitch period in frames: {}".format(pitch_period))
    pwr_frame = analysed_signal[-pitch_period:]

    predicted_frame = np.zeros(len(frame_minus_one))

    pwr_frame_index = 0
    for i in range(len(predicted_frame)):
        predicted_frame[i] = pwr_frame[pwr_frame_index]

        if pwr_frame_index >= len(pwr_frame)-1:
            pwr_frame_index = 0
        else:
            pwr_frame_index = pwr_frame_index + 1

    quarter_pitch_period_samples = int(pitch_period/4)
    pre_pwr_quarter_pitch_signal_original = frame_minus_one[-quarter_pitch_period_samples:]
    post_pwr_quarter_pitch_signal_original = frame_plus_one[:quarter_pitch_period_samples]

    pre_pwr_quarter_pitch_signal_pitchperiod = pwr_frame[-quarter_pitch_period_samples:]
    post_pwr_quarter_pitch_signal_pitchperiod = np.zeros(quarter_pitch_period_samples)
    for i in range(quarter_pitch_period_samples):
        post_pwr_quarter_pitch_signal_pitchperiod[i] = pwr_frame[pwr_frame_index]
        if pwr_frame_index >= len(pwr_frame)-1:
            pwr_frame_index = 0
        else:
            pwr_frame_index = pwr_frame_index + 1

    hanning_window = np.hanning(2 * quarter_pitch_period_samples + 1)
    hanning_window = hanning_window[:int(len(hanning_window) / 2)]
    modified_tail_of_frame_minus_one = []
    modified_head_of_frame_plus_one = []
    for i in range(quarter_pitch_period_samples):
        modified_pre_pwr_quarter_pitch_signal_original = pre_pwr_quarter_pitch_signal_original[i] * hanning_window[-i - 1]
        modified_pre_pwr_quarter_pitch_signal_pitchperiod = pre_pwr_quarter_pitch_signal_pitchperiod[i] * hanning_window[i]
        pre_pwr_quarter_pitch_signal_final = modified_pre_pwr_quarter_pitch_signal_original + modified_pre_pwr_quarter_pitch_signal_pitchperiod
        modified_tail_of_frame_minus_one = np.append(modified_tail_of_frame_minus_one, pre_pwr_quarter_pitch_signal_final)
    frame_minus_one[len(frame_minus_one) - quarter_pitch_period_samples:] = modified_tail_of_frame_minus_one

    for i in range(quarter_pitch_period_samples):
        modified_post_pwr_quarter_pitch_signal_original = post_pwr_quarter_pitch_signal_original[i] * hanning_window[i]
        modified_post_pwr_quarter_pitch_signal_pitchperiod = post_pwr_quarter_pitch_signal_pitchperiod[i] * hanning_window[-i - 1]
        post_pwr_quarter_pitch_signal_final = modified_post_pwr_quarter_pitch_signal_original + modified_post_pwr_quarter_pitch_signal_pitchperiod
        modified_head_of_frame_plus_one = np.append(modified_head_of_frame_plus_one, post_pwr_quarter_pitch_signal_final)
    frame_plus_one[:quarter_pitch_period_samples] = modified_head_of_frame_plus_one

    return frame_minus_one, predicted_frame, frame_plus_one, pitch_period


def plc_algorithm(frame_minus_one: np.ndarray, frame_minus_two: np.ndarray, frame_plus_one: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:

    # Pitch detection
    max_abs_signal1 = abs(max(frame_minus_one, key=abs))
    if max_abs_signal1 != 0:
        normalized_signal1 = frame_minus_one / max_abs_signal1
    else:
        normalized_signal1 = frame_minus_one
    max_abs_signal2 = abs(max(frame_minus_two, key=abs))
    if max_abs_signal2 != 0:
        normalized_signal2 = frame_minus_two / max_abs_signal2
    else:
        normalized_signal2 = frame_minus_two

    normalized_cross_correlation = scipy.signal.correlate(normalized_signal1, normalized_signal2, method='fft')
    max_normalized_cross_correlation_index = np.argmax(normalized_cross_correlation)
    print(max_normalized_cross_correlation_index)

    pitch_period_samples = len(normalized_cross_correlation) - max_normalized_cross_correlation_index
    quarter_pitch_period_samples = int(pitch_period_samples/4)
    hanning_window = np.hanning(2*quarter_pitch_period_samples + 1)
    hanning_window = hanning_window[:int(len(hanning_window)/2)]

    pitch_period_frame = np.append(frame_minus_two, frame_minus_one)[-pitch_period_samples:]

    # Modifies tail of last good frame, to ensure smooth transition to synthetic generated signal
    tail_of_frame_minus_one = frame_minus_one[len(frame_minus_one)-quarter_pitch_period_samples:]
    modified_tail_of_frame_minus_one = np.asarray([])
    for i in range(len(tail_of_frame_minus_one)):
        modified_sample_tail = tail_of_frame_minus_one[i]*hanning_window[-i-1]
        modified_sample_pitch_frame = pitch_period_frame[-quarter_pitch_period_samples+i]*hanning_window[i]
        modified_sample_synthetic_signal = modified_sample_tail + modified_sample_pitch_frame
        modified_tail_of_frame_minus_one = np.append(modified_tail_of_frame_minus_one, modified_sample_synthetic_signal)
    frame_minus_one[len(frame_minus_one) - quarter_pitch_period_samples:] = modified_tail_of_frame_minus_one

    # Construction of synthetic signal
    synthetic_signal = np.zeros(len(frame_minus_one))
    index_pitch_period_frame = 0
    for i in range(len(synthetic_signal)):
        synthetic_signal[i] = pitch_period_frame[index_pitch_period_frame]
        index_pitch_period_frame = index_pitch_period_frame + 1
        if index_pitch_period_frame >= len(pitch_period_frame):
            index_pitch_period_frame = 0

    # Modifies head of following frame (for now it's always considered a good frame, gonna change in v02)
    for i in range(quarter_pitch_period_samples):
        modified_sample_head = frame_plus_one[i]*hanning_window[i]
        modified_sample_pitch_frame = pitch_period_frame[index_pitch_period_frame]*hanning_window[-i-1]
        index_pitch_period_frame = index_pitch_period_frame + 1
        if index_pitch_period_frame >= len(pitch_period_frame):
            index_pitch_period_frame = 0
        frame_plus_one[i] = modified_sample_head + modified_sample_pitch_frame

    return frame_minus_one, synthetic_signal, frame_plus_one
