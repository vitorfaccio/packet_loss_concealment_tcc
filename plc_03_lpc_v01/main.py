import os

from plc_lpc_forward import *
from audio_manipulation.read_audiofile_as_frames import *

CORRECTED_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'corrected_lpc_v1'

# ARQUIVO NÃO UTILIZADO.
# Algoritmo baseado em LPC é implementado em sentidos
# forward e backward na pasta "plc_05_lpc_v03_each_side".

if __name__ == '__main__':

    # Variables setup
    # G.711 uses frames of 10ms
    frame_length_ms = 20
    samplerate = 16000  # Expected samplerate for given files

    try:
        # Iterates over wav file paths in './audios/' folder:
        all_audio_paths = sorted(Path(os.getcwd()).parent.resolve().glob('TCD-VOIP_for_use/chop_20ms_160ms_16khz/*.wav'))
        for audio_path in all_audio_paths:

            # Reads audio file as list of frames:
            frames, sr = read_audiofile_as_frames(audio_path, frame_length_ms)
            frame_length_samples = ms_to_sample(frame_length_ms, sr)

            # Run audio frames
            for frame_index in range(1, len(frames)-1):

                # Detects if frame is valid or not:
                if not get_frame_validity(frames[frame_index]):
                    frames[frame_index] = plc_algorithm_lpc_elsabrouty_eq4(frames[frame_index - 2], frames[frame_index - 1], samplerate)
                    # frames[frame_index] = plc_algorithm_lpc_elsabrouty_eq4_corrected(frames[frame_index - 2], frames[frame_index - 1], samplerate)

            # Save file
            print("Starting file save")
            new_file_name = audio_path.name
            new_file_name = 'CORRECTED_' + new_file_name
            frames_to_save = np.reshape(frames, (1, frame_length_samples*len(frames)))[0]
            frames_to_save = frames_to_save.astype('int16')
            sf.write(CORRECTED_SET_DIR / new_file_name, frames_to_save, sr)

            break

    except Exception as e:
        print("Error occurred: {}".format(e))
