import librosa
import numpy as np
from matplotlib import pyplot as plt

# Original signal:
original_signal = np.arange(628, dtype='float')
for i in range(len(original_signal)):
    original_signal[i] = np.sin(i*0.01) + 0.75*np.sin(i*0.03) + 0.5*np.sin(i*0.05) + 0.25*np.sin(i*0.11)


for lpc_order in range(1, 30):
    # LP coefficients:
    lpc_coefficients = librosa.lpc(original_signal, order=lpc_order)
    # lpc_order = 10

    # Predicted signal:
    predicted_signal = np.zeros(len(original_signal))
    predicted_signal[:314] = original_signal[:314]
    # for i in range(lpc_order, len(predicted_signal)):
    for i in range(314, 628):
        for j in range(lpc_order):
            predicted_signal[i] = predicted_signal[i] - (lpc_coefficients[j] * original_signal[i - 1 - j])

    fig, (ax_0, ax_1) = plt.subplots(2, 1, figsize=(10, 10))
    ax_0.set_title(f'LPC order: {lpc_order}')
    ax_0.plot(original_signal, 'b', label='Original signal')
    ax_1.plot(predicted_signal, 'r', label='Predicted signal')
    ax_0.margins(0, 0.1)
    ax_1.margins(0, 0.1)
    fig.tight_layout()
    plt.show(block=True)
