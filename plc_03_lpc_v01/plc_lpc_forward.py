import librosa
import numpy as np
import scipy.signal
from matplotlib import pyplot as plt

from audio_manipulation.read_audiofile_as_frames import *


# Não funciona direito, predicted signal é instável e estoura
def plc_algorithm_lpc_kondo(frame_minus_one: np.ndarray, frame_plus_one: np.ndarray, samplerate: int) -> np.ndarray:
    lpc_order = 40
    lpc_analysis_window_length = ms_to_sample(20, samplerate)
    lpc_fwd_samples_window = np.flip(frame_minus_one[-lpc_analysis_window_length:])

    lpc_fwd_coefficients = librosa.lpc(lpc_fwd_samples_window, order=lpc_order)  # LPC analysis length: 1 frame
    predicted_frame_fwd = np.zeros(len(frame_minus_one))

    predicted_value = 0
    for i in range(1, lpc_order):
        single_value = lpc_fwd_coefficients[i] * frame_minus_one[-i-1]
        predicted_value += single_value

    predicted_frame_fwd[0] = predicted_value * (-1)

    for k in range(1, len(predicted_frame_fwd)):
        predicted_value = 0
        for j in range(1, lpc_order):
            if j <= k:
                single_value = lpc_fwd_coefficients[j] * predicted_frame_fwd[k-j]
            else:
                single_value = lpc_fwd_coefficients[j] * frame_minus_one[-j+k-1]
            predicted_value += single_value
        predicted_frame_fwd[k] = predicted_value * (-1)

    return predicted_frame_fwd


def get_pitch_period_signal(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray) -> np.ndarray:
    # Pitch detection
    max_abs_signal1 = abs(max(frame_minus_one, key=abs))
    if max_abs_signal1 != 0:
        normalized_signal1 = frame_minus_one / max_abs_signal1
    else:
        normalized_signal1 = frame_minus_one
    max_abs_signal2 = abs(max(frame_minus_two, key=abs))
    if max_abs_signal2 != 0:
        normalized_signal2 = frame_minus_two / max_abs_signal2
    else:
        normalized_signal2 = frame_minus_two

    normalized_cross_correlation = scipy.signal.correlate(normalized_signal1, normalized_signal2, method='fft')
    max_normalized_cross_correlation_index = np.argmax(normalized_cross_correlation)
    pitch_period_samples = len(normalized_cross_correlation) - max_normalized_cross_correlation_index
    pitch_period_signal = np.append(frame_minus_two, frame_minus_one)[-pitch_period_samples:]

    return pitch_period_signal


def plc_algorithm_lpc_elsabrouty_eq2(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray, samplerate: int) -> np.ndarray:
    G = 0.01
    lpc_order = 50
    lpc_analysis_window_length = ms_to_sample(20, samplerate)
    lpc_fwd_samples_window = np.flip(frame_minus_one[-lpc_analysis_window_length:])

    lpc_fwd_coefficients = librosa.lpc(lpc_fwd_samples_window, order=lpc_order)  # LPC analysis length: 1 frame
    predicted_frame_fwd = np.zeros(len(frame_minus_one))
    # predicted_frame_signal = np.append(frame_minus_one, np.zeros(len(frame_minus_one)))

    pwr_frame = get_pitch_period_signal(frame_minus_two, frame_minus_one)
    predicted_value = 0
    for i in range(1, lpc_order):
        single_value = lpc_fwd_coefficients[i] * frame_minus_one[-i-1]
        predicted_value += single_value
    predicted_frame_fwd[0] = predicted_value*(-1) + pwr_frame[0]*G

    pwr_frame_index = 1
    for k in range(1, len(predicted_frame_fwd)):
        predicted_value = 0
        for j in range(1, lpc_order):
            if j <= k:
                single_value = lpc_fwd_coefficients[j] * predicted_frame_fwd[k-j]
            else:
                single_value = lpc_fwd_coefficients[j] * frame_minus_one[k-j-1]
            predicted_value += single_value
        predicted_frame_fwd[k] = predicted_value*(-1) + pwr_frame[pwr_frame_index]*G
        if pwr_frame_index >= len(pwr_frame)-1:
            pwr_frame_index = 0
        else:
            pwr_frame_index = pwr_frame_index + 1
    return predicted_frame_fwd


def plc_algorithm_lpc_elsabrouty_eq4(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray, samplerate: int) -> np.ndarray:
    G = 0.01
    alpha = 0.7
    beta = 0.3
    lpc_order = 50
    lpc_analysis_window_length = ms_to_sample(20, samplerate)
    lpc_fwd_samples_window = np.flip(frame_minus_one[-lpc_analysis_window_length:])

    lpc_fwd_coefficients = librosa.lpc(lpc_fwd_samples_window, order=lpc_order)  # LPC analysis length: 1 frame
    predicted_frame_fwd = np.zeros(len(frame_minus_one))

    pwr_frame = get_pitch_period_signal(frame_minus_two, frame_minus_one)
    predicted_value = 0
    for i in range(1, lpc_order):
        single_value = lpc_fwd_coefficients[i] * frame_minus_one[-i-1]
        predicted_value += single_value
    predicted_frame_fwd[0] = (predicted_value + pwr_frame[0]*G)*alpha*(-1) + pwr_frame[0]*beta

    pwr_frame_index = 1
    for k in range(1, len(predicted_frame_fwd)):
        predicted_value = 0
        for j in range(1, lpc_order):
            if j <= k:
                single_value = lpc_fwd_coefficients[j] * predicted_frame_fwd[k-j]
            else:
                single_value = lpc_fwd_coefficients[j] * frame_minus_one[k-j-1]
            predicted_value += single_value
        predicted_frame_fwd[k] = (predicted_value + pwr_frame[pwr_frame_index]*G)*alpha*(-1) + pwr_frame[pwr_frame_index]*beta
        if pwr_frame_index >= len(pwr_frame)-1:
            pwr_frame_index = 0
        else:
            pwr_frame_index = pwr_frame_index + 1
    return predicted_frame_fwd


# Texto discrimina o S[n] de S1[n] nas equações 3 e 4, o que faz com que a recursão não seja sobre a
# última amostra predita, e sim sobre apenas um termo dela.
def plc_algorithm_lpc_elsabrouty_eq4_corrected(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray, samplerate: int) -> np.ndarray:
    G = 0.01
    alpha = 0.7
    beta = 0.3
    lpc_order = 50
    lpc_analysis_window_length = ms_to_sample(20, samplerate)
    lpc_fwd_samples_window = np.flip(frame_minus_one[-lpc_analysis_window_length:])

    lpc_fwd_coefficients = librosa.lpc(lpc_fwd_samples_window, order=lpc_order)  # LPC analysis length: 1 frame
    s_n = np.zeros(len(frame_minus_one))
    s_1_n = np.zeros(len(frame_minus_one))

    pwr_frame = get_pitch_period_signal(frame_minus_two, frame_minus_one)
    immediate_sum = 0
    for i in range(1, lpc_order):
        single_value = lpc_fwd_coefficients[i] * frame_minus_one[-i-1]
        immediate_sum += single_value
    s_1_n[0] = (immediate_sum + pwr_frame[0]*G)*(-1)
    s_n[0] = s_1_n[0]*alpha + pwr_frame[0]*beta

    pwr_frame_index = 1
    for k in range(1, len(s_n)):
        immediate_sum = 0
        for j in range(1, lpc_order):
            if j <= k:
                single_value = lpc_fwd_coefficients[j] * s_1_n[k-j]
            else:
                single_value = lpc_fwd_coefficients[j] * frame_minus_one[k-j-1]
            immediate_sum += single_value
        s_1_n[k] = (immediate_sum + pwr_frame[pwr_frame_index]*G)*(-1)
        s_n[k] = s_1_n[k]*alpha + pwr_frame[pwr_frame_index]*beta
        if pwr_frame_index >= len(pwr_frame)-1:
            pwr_frame_index = 0
        else:
            pwr_frame_index = pwr_frame_index + 1
    return s_n


def plc_algorithm_lpc_collomb_1(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray, samplerate: int) -> np.ndarray:
    for lpc_order in range(1, 50):

        # LP coefficients:
        lpc_coefficients = librosa.lpc(frame_minus_one, order=lpc_order)

        # Predicted signal:
        predicted_signal = np.append(frame_minus_one, np.zeros(len(frame_minus_one)))
        # predicted_signal[:len(frame_minus_one)] = frame_minus_one[:314]
        # for i in range(lpc_order, len(predicted_signal)):
        for i in range(len(frame_minus_one), len(predicted_signal)):
            for j in range(lpc_order):
                predicted_signal[i] = predicted_signal[i] - (lpc_coefficients[j] * predicted_signal[i - 1 - j])

        predicted_signal = predicted_signal[len(frame_minus_one):len(predicted_signal)]
        fig, (ax_0, ax_1) = plt.subplots(2, 1, figsize=(10, 10))
        ax_0.set_title(f'LPC order: {lpc_order}')
        ax_0.plot(frame_minus_one, 'b', label='Original signal')
        ax_1.plot(predicted_signal, 'r', label='Predicted signal')
        ax_0.margins(0, 0.1)
        ax_1.margins(0, 0.1)
        fig.tight_layout()
        plt.show(block=True)


def plc_algorithm_lpc_collomb_original(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray, samplerate: int) -> np.ndarray:
    for lpc_order in range(1, 50):
        # frame_minus_one = np.arange(len(frame_minus_one), dtype='float')
        # for i in range(len(frame_minus_one)):
        #     frame_minus_one[i] = np.sin(i * 0.01) + 0.75 * np.sin(i * 0.03) + 0.5 * np.sin(i * 0.05) + 0.25 * np.sin(
        #         i * 0.11)

        # LP coefficients:
        lpc_coefficients = librosa.lpc(frame_minus_one, order=lpc_order)

        # Predicted signal:
        predicted_signal = np.zeros(len(frame_minus_one))
        for i in range(lpc_order, len(predicted_signal)):
            for j in range(lpc_order):
                predicted_signal[i] = predicted_signal[i] - (lpc_coefficients[j] * frame_minus_one[i - 1 - j])

        fig, (ax_0, ax_1, ax_2) = plt.subplots(3, 1, figsize=(10, 10))
        ax_0.set_title(f'LPC order: {lpc_order}')
        ax_0.plot(frame_minus_one, 'b', label='Original signal')
        ax_1.plot(predicted_signal, 'r', label='Predicted signal')
        ax_2.plot(frame_minus_one, 'b', label='Original signal')
        ax_2.plot(-predicted_signal, 'r', label='Predicted signal')
        ax_0.margins(0, 0.1)
        ax_1.margins(0, 0.1)
        ax_2.margins(0, 0.1)
        fig.tight_layout()
        plt.show(block=True)


def plc_algorithm_lpc_collomb_recursive(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray, samplerate: int) -> np.ndarray:
    for lpc_order in range(1, 50):
        frame_minus_one = np.arange(len(frame_minus_one), dtype='float')
        for i in range(len(frame_minus_one)):
            frame_minus_one[i] = np.sin(i * 0.01) + 0.75 * np.sin(i * 0.03) + 0.5 * np.sin(i * 0.05) + 0.25 * np.sin(
                i * 0.11)

        # LP coefficients:
        lpc_coefficients = librosa.lpc(frame_minus_one[-50:], order=lpc_order)

        # Predicted signal:
        predicted_signal = np.append(frame_minus_one, np.zeros(len(frame_minus_one)))
        for k in range(len(frame_minus_one), len(predicted_signal)):
            for j in range(lpc_order):
                predicted_signal[k] = predicted_signal[k] - (lpc_coefficients[j] * predicted_signal[k - 1 - j])
        predicted_signal = predicted_signal[len(frame_minus_one):len(predicted_signal)]
        fig, (ax_0, ax_1, ax_2) = plt.subplots(3, 1, figsize=(10, 10))
        ax_0.set_title(f'LPC order: {lpc_order}')
        ax_0.plot(frame_minus_one, 'b', label='Original signal')
        ax_1.plot(predicted_signal, 'r', label='Predicted signal')
        ax_2.plot(np.append(frame_minus_one, -predicted_signal), 'b', label='Complete signal')
        ax_0.margins(0, 0.1)
        ax_1.margins(0, 0.1)
        ax_2.margins(0, 0.1)
        fig.tight_layout()
        plt.show(block=True)







##################################################################
def plc_algorithm_lpc(frame_minus_one: np.ndarray, frame_plus_one: np.ndarray) -> np.ndarray:
    lpc_order = 40
    lpc_coefficients = librosa.lpc(frame_plus_one, order=lpc_order)
    print(lpc_coefficients)
    predicted_frame = np.zeros(len(frame_minus_one))
    for n in range(len(frame_minus_one)):
        predicted_x_n = 0.
        for i in range(lpc_order):
            a_i = lpc_coefficients[i]
            x_n_i = frame_minus_one[n - i]
            single_value = a_i * x_n_i
            predicted_x_n = predicted_x_n - single_value
        predicted_frame[n] = predicted_x_n
    return predicted_frame


def pitch_detection_as_described_in_xie_wei(pitch_buffer: np.ndarray, samplerate: int) -> None:

    min_pitch_period = ms_to_sample(5, samplerate)
    max_pitch_period = ms_to_sample(15, samplerate)

    arg_max = 0
    for i in range(min_pitch_period, max_pitch_period+1):
        pass

    pitch = None
    return pitch
