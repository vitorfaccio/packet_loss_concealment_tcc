import os

from scipy import fftpack
from matplotlib import pyplot as plt

from audio_manipulation.read_audiofile_as_frames import *
from pitch_waveform_replication import *

# Original signal:
samplerate = 16000
T_frame = 0.02
T_total = 0.08
l_frame = int(T_frame * samplerate)
l_total = int(T_total * samplerate)

# Obtaining demonstration signal
audio_path = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'reference_16khz' / 'R_01_CHOP_FA.wav'
full_frames, samplerate_obtained = read_audiofile_as_frames(audio_path, 1000*T_frame)
assert samplerate == samplerate_obtained

selected_frames = full_frames[303:307]
original_signal = np.reshape(selected_frames, (1, l_frame*len(selected_frames)))[0]
demo_signal = np.zeros(l_total, dtype='float')
demo_signal[:int(2*l_frame)] = original_signal[:int(2*l_frame)]
demo_signal[int(3*l_frame):] = original_signal[int(3*l_frame):]

#####################################
# Insert signal processing here

# Signal processing until here
#####################################

plot_sinal_DT = True
plot_sinal_DF = True

time_axis = np.linspace(0, T_total, l_total, endpoint=False)
major_ticks = np.arange(0, T_total, T_frame)
minor_ticks = np.arange(0, T_total, 0.01)

T = 1.0 / samplerate
N = len(original_signal)
xf = np.linspace(0.0, 1.0 / (2.0 * T), N // 2)
minor_ticks_FFT = np.linspace(0.0, 1.0 / (2.0 * T), 17)

# Gráficos do sinal original
if plot_sinal_DT:
    fig, ax_0 = plt.subplots(figsize=(10, 6))

    ax_0.axvspan(0.04, 0.06, facecolor='r', alpha=0.15)
    ax_0.axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
    ax_0.plot(time_axis, original_signal, 'b', label='Sinal de demonstração original')
    ax_0.set_xticks(major_ticks)
    ax_0.set_xticks(minor_ticks, minor=True)
    ax_0.grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
    ax_0.grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
    ax_0.set_ylabel('Amplitude')
    ax_0.set_xlabel('Tempo (s)')
    ax_0.margins(0, 0.1)
    plt.tight_layout()
    plt.show(block=True)

if plot_sinal_DF:
    fig, ax_1 = plt.subplots(figsize=(10, 6))

    yf_original = fftpack.fft(original_signal)
    ax_1.plot(xf, 2.0 / N * np.abs(yf_original[:N // 2]), label='FFT')
    ax_1.set_ylabel('FFT')
    ax_1.set_xlabel('Frequência (Hz)')
    ax_1.margins(0, 0.1)
    plt.tight_layout()
    plt.show(block=True)


# Gráficos do sinal com picote
if plot_sinal_DT:
    fig, ax_2 = plt.subplots(figsize=(10, 6))
    ax_2.axvspan(0.04, 0.06, facecolor='r', alpha=0.15)
    ax_2.axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
    ax_2.plot(time_axis, demo_signal, 'b', label='Sinal de demonstração com picote')
    ax_2.set_xticks(major_ticks)
    ax_2.set_xticks(minor_ticks, minor=True)
    ax_2.grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
    ax_2.grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
    ax_2.set_ylabel('Amplitude')
    ax_2.set_xlabel('Tempo (s)')
    ax_2.margins(0, 0.1)
    plt.tight_layout()
    plt.show(block=True)

if plot_sinal_DF:
    fig, ax_3 = plt.subplots(figsize=(10, 6))
    yf_corrected = fftpack.fft(demo_signal)
    ax_3.plot(xf, 2.0 / N * np.abs(yf_corrected[:N // 2]), label='FFT')
    ax_3.set_ylabel('FFT')
    ax_3.set_xlabel('Frequência (Hz)')
    ax_3.margins(0, 0.1)
    plt.tight_layout()
    plt.show(block=True)
