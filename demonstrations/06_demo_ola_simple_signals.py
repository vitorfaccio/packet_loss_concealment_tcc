import os

import numpy as np
from scipy import fftpack
from matplotlib import pyplot as plt

from audio_manipulation.read_audiofile_as_frames import *
from overlap_add import *

# Original signal:
samplerate = 16000
T_frame = 0.02
T_total = 0.06
l_frame = int(T_frame * samplerate)
l_total = int(T_total * samplerate)

# Obtaining demonstration signal
audio_path = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'reference_16khz' / 'R_01_CHOP_FA.wav'
full_frames, samplerate_obtained = read_audiofile_as_frames(audio_path, 1000*T_frame)
assert samplerate == samplerate_obtained

# selected_frames = full_frames[303:307]
frame_inicial = 311
# selected_frames = full_frames[frame_inicial:frame_inicial+4]
selected_frames = full_frames[frame_inicial:frame_inicial+5]
original_signal = np.reshape(selected_frames, (1, l_frame*len(selected_frames)))[0]
demo_signal = original_signal
# demo_signal[:int(2*l_frame)] = original_signal[:int(2*l_frame)]
# demo_signal[int(3*l_frame):] = original_signal[int(3*l_frame):]

#####################################
# Insert signal processing here

frames = read_signal_as_frames(demo_signal, T_frame*1000, samplerate)

signal_1 = np.append(frames[0], frames[1])
signal_1 = np.append(signal_1, np.zeros(l_frame))

signal_2 = np.append(np.zeros(l_frame), frames[3])
signal_2 = np.append(signal_2, frames[4])

x_1_n = signal_1[l_frame:2*l_frame]
x_2_n = signal_2[l_frame:2*l_frame]

y_n = ola_simple(frames[1], frames[2])
signal_result = np.append(np.zeros(l_frame), y_n)
signal_result = np.append(signal_result, np.zeros(l_frame))


# Signal processing until here
#####################################

fig, axs = plt.subplots(2, 1, figsize=(15, 10))

time_axis = np.linspace(0, T_frame*3, l_frame*3, endpoint=False)
major_ticks = np.arange(0, T_frame*3, T_frame)
minor_ticks = np.arange(0, T_frame*3, T_frame/2)

axs[0].axvspan(0.02, 0.04, facecolor='b', alpha=0.2)
axs[0].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
axs[0].plot(time_axis, signal_1, 'b', label='Sinal 1')
axs[0].set_xticks(major_ticks)
axs[0].set_xticks(minor_ticks, minor=True)
axs[0].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
axs[0].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
axs[0].set_ylabel('Amplitude')
axs[0].set_xlabel('Tempo (s)')
axs[0].set_title('Sinal 1')
axs[0].margins(0, 0.1)

axs[1].axvspan(0.02, 0.04, facecolor='b', alpha=0.2)
axs[1].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
axs[1].plot(time_axis, signal_2, 'b', label='Sinal 2')
axs[1].set_xticks(major_ticks)
axs[1].set_xticks(minor_ticks, minor=True)
axs[1].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
axs[1].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
axs[1].set_ylabel('Amplitude')
axs[1].set_xlabel('Tempo (s)')
axs[1].set_title('Sinal 2')
axs[1].margins(0, 0.1)

# axs[2].axvspan(0.02, 0.04, facecolor='b', alpha=0.2)
# axs[2].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
# axs[2].plot(time_axis, signal_result, 'b', label='Sinal resultante')
# # axs[2].set_xticks(major_ticks)
# # axs[2].set_xticks(minor_ticks, minor=True)
# axs[2].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
# axs[2].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
# axs[2].set_ylabel('Amplitude')
# axs[2].set_xlabel('Tempo (s)')
# axs[2].set_title('Sinal resultante)')
# axs[2].margins(0, 0.1)



# Gráficos de FFT dos sinais
# T = 1.0 / samplerate
# N = len(original_signal)
# xf = np.linspace(0.0, 1.0 / (2.0 * T), N // 2)
#
# yf_original = fftpack.fft(original_signal)
# axs[0, 1].plot(xf, 2.0 / N * np.abs(yf_original[:N // 2]), label='FFT')
# axs[0, 1].set_ylabel('FFT')
# axs[0, 1].set_xlabel('Frequência (Hz)')
# axs[0, 1].set_title('Sinal original - FFT')
# axs[0, 1].margins(0, 0.1)
#
# yf_corrected = fftpack.fft(corrected_signal)
# axs[1, 1].plot(xf, 2.0 / N * np.abs(yf_corrected[:N // 2]), label='FFT')
# axs[1, 1].set_ylabel('FFT')
# axs[1, 1].set_xlabel('Frequência (Hz)')
# axs[1, 1].set_title('Sinal corrigido - FFT')
# axs[1, 1].margins(0, 0.1)

plt.tight_layout()
plt.show(block=True)
