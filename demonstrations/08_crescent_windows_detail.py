import os

import numpy as np
from scipy import fftpack
from matplotlib import pyplot as plt

from audio_manipulation.read_audiofile_as_frames import *
from overlap_add import *

# Original signal:
samplerate = 16000
T_frame = 0.02
T_total = 0.02
l_frame = int(T_frame * samplerate)
l_total = int(T_total * samplerate)
window_length = l_total

hanning_window_full = np.hanning(2*window_length + 1)
hanning_window = hanning_window_full[:int(len(hanning_window_full)/2)]

triangular_window = np.linspace(0., 1., window_length)


fig, axs = plt.subplots(1, 2, figsize=(16, 5))

samples_axis = np.linspace(0, l_total, l_total, endpoint=False)
major_ticks = np.arange(0, l_total, l_total/4)
minor_ticks = np.arange(0, l_total, l_total/8)

axs[0].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
axs[0].plot(samples_axis, triangular_window, 'b', label='Janela triangular')
axs[0].set_xticks(major_ticks)
axs[0].set_xticks(minor_ticks, minor=True)
axs[0].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
axs[0].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
axs[0].set_ylabel('Amplitude')
axs[0].set_xlabel('Amostras (n)')
axs[0].set_title('Janela triangular')
axs[0].margins(0, 0.1)

axs[1].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
axs[1].plot(samples_axis, hanning_window, 'b', label='Janela Hanning')
axs[1].set_xticks(major_ticks)
axs[1].set_xticks(minor_ticks, minor=True)
axs[1].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
axs[1].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
axs[1].set_ylabel('Amplitude')
axs[1].set_xlabel('Amostras (n)')
axs[1].set_title('Janela Hanning')
axs[1].margins(0, 0.1)

plt.tight_layout()
plt.show(block=True)
