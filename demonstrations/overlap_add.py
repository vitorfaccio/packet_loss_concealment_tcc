import numpy as np


def ola_simple(x_1_n: np.ndarray, x_2_n: np.ndarray) -> np.ndarray:
    y_n = np.zeros(len(x_1_n))
    for index, (x_1_i, x_2_i) in enumerate(zip(x_1_n, x_2_n)):
        y_n[index] = x_1_i + x_2_i

    return np.asarray(y_n)
