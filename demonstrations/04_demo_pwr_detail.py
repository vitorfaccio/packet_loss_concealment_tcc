import os

from scipy import fftpack
from matplotlib import pyplot as plt

from audio_manipulation.read_audiofile_as_frames import *
from pitch_waveform_replication import *

# Original signal:
samplerate = 16000
T_frame = 0.02
T_total = 0.08
l_frame = int(T_frame * samplerate)
l_total = int(T_total * samplerate)

# Obtaining demonstration signal
audio_path = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'reference_16khz' / 'R_01_CHOP_FA.wav'
full_frames, samplerate_obtained = read_audiofile_as_frames(audio_path, 1000*T_frame)
assert samplerate == samplerate_obtained

# selected_frames = full_frames[304:305]
selected_frames = full_frames[303:307]
original_signal = np.reshape(selected_frames, (1, l_frame*len(selected_frames)))[0]
demo_signal = np.zeros(l_total, dtype='float')
demo_signal[:int(2*l_frame)] = original_signal[:int(2*l_frame)]
demo_signal[int(3*l_frame):] = original_signal[int(3*l_frame):]

#####################################
# Insert signal processing here

frames = read_signal_as_frames(demo_signal, T_frame*1000, samplerate)
analysed_signal = np.append(frames[0][-120:], frames[1])

frames[2], pitch_period = pitch_waveform_replication(analysed_signal, l_frame)
frames[2][pitch_period:] = np.zeros(l_frame-pitch_period, dtype='float')
frames[3] = np.zeros(l_frame, dtype='float')
corrected_signal = np.reshape(frames, (1, l_frame*len(frames)))[0][640-pitch_period:641+pitch_period]

# Signal processing until here
#####################################

fig, axs = plt.subplots(2, 1, figsize=(10, 7))

time_axis = np.linspace(0, T_total, l_total, endpoint=False)
major_ticks_0 = np.arange(0, T_total, T_frame)
minor_ticks_0 = np.arange(0, T_total, 0.01)

# Gráficos de sinal no domínio do tempo
axs[0].axvspan((0.04 - (pitch_period/samplerate)), 0.04, facecolor='g', alpha=0.2)
axs[0].axvspan((200/samplerate), (0.04 - (pitch_period/samplerate)), facecolor='b', alpha=0.1)
axs[0].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
axs[0].plot(time_axis, demo_signal, 'b', label='Sinal de demonstração')
axs[0].set_xticks(major_ticks_0)
axs[0].set_xticks(minor_ticks_0, minor=True)
axs[0].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
axs[0].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
axs[0].set_ylabel('Amplitude')
axs[0].set_xlabel('Tempo (s)')
axs[0].set_title('Sinal de demonstração')
axs[0].margins(0, 0.1)

major_ticks_1 = np.arange(0, 440, 50)
minor_ticks_1 = np.arange(0, 440, 440-pitch_period)
axs[1].axvspan(440-pitch_period, 440, facecolor='g', alpha=0.2)
axs[1].axvspan(0, 440-pitch_period, facecolor='b', alpha=0.1)
axs[1].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
axs[1].plot(analysed_signal, 'b', label='Sinal analisado por detecção de período de pitch')
axs[1].set_xticks(major_ticks_1)
axs[1].set_xticks(minor_ticks_1, minor=True)
axs[1].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
axs[1].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
axs[1].set_ylabel('Amplitude')
axs[1].set_xlabel('Amostras (n)')
axs[1].set_title('Sinal analisado por detecção de período de pitch')
axs[1].margins(0, 0.1)

axs[1].tick_params(axis='x', which='minor')


# Gráficos de FFT dos sinais
# T = 1.0 / samplerate
# N = len(original_signal)
# xf = np.linspace(0.0, 1.0 / (2.0 * T), N // 2)
#
# yf_original = fftpack.fft(original_signal)
# axs[0, 1].plot(xf, 2.0 / N * np.abs(yf_original[:N // 2]), label='FFT')
# axs[0, 1].set_ylabel('FFT')
# axs[0, 1].set_xlabel('Frequência (Hz)')
# axs[0, 1].set_title('Sinal original - FFT')
# axs[0, 1].margins(0, 0.1)
#
# yf_corrected = fftpack.fft(corrected_signal)
# axs[1, 1].plot(xf, 2.0 / N * np.abs(yf_corrected[:N // 2]), label='FFT')
# axs[1, 1].set_ylabel('FFT')
# axs[1, 1].set_xlabel('Frequência (Hz)')
# axs[1, 1].set_title('Sinal corrigido - FFT')
# axs[1, 1].margins(0, 0.1)

plt.tight_layout()
plt.show(block=True)
