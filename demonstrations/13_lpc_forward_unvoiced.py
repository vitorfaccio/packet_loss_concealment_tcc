import os

from matplotlib import pyplot as plt

from audio_manipulation.read_audiofile_as_frames import *
from linear_prediction_coding import *

# Original signal:
samplerate = 16000
T_frame = 0.02
T_total = 0.08
l_frame = int(T_frame * samplerate)
l_total = int(T_total * samplerate)

# Obtaining demonstration signal
audio_path = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'reference_16khz' / 'R_01_CHOP_FA.wav'
full_frames, samplerate_obtained = read_audiofile_as_frames(audio_path, 1000*T_frame)
assert samplerate == samplerate_obtained

frame_inicial = 313
selected_frames = full_frames[frame_inicial:frame_inicial+4]
original_signal = np.reshape(selected_frames, (1, l_frame*len(selected_frames)))[0]
demo_signal = np.zeros(l_total, dtype='float')
demo_signal[:int(2*l_frame)] = original_signal[:int(2*l_frame)]
demo_signal[int(3*l_frame):] = original_signal[int(3*l_frame):]

#####################################
# Insert signal processing here

frames = read_signal_as_frames(demo_signal, T_frame*1000, samplerate)
frames[2] = elsabrouty_eq_4(frames[1])
corrected_signal = np.reshape(frames, (1, l_frame*len(frames)))[0]
difference_signal = corrected_signal - original_signal

# Signal processing until here
#####################################

fig, axs = plt.subplots(3, 1, figsize=(10, 10))

time_axis = np.linspace(0, T_total, l_total, endpoint=False)
major_ticks = np.arange(0, T_total, T_frame)
minor_ticks = np.arange(0, T_total, 0.01)

# Gráficos de sinal no domínio do tempo
axs[0].axvspan(0.04, 0.06, facecolor='r', alpha=0.15)
# axs[0].axvspan((0.04 - (pitch_period/samplerate)), 0.04, facecolor='g', alpha=0.2)
axs[0].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
axs[0].plot(time_axis, original_signal, 'b', label='Sinal original')
axs[0].set_xticks(major_ticks)
axs[0].set_xticks(minor_ticks, minor=True)
axs[0].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
axs[0].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
axs[0].set_ylabel('Amplitude')
axs[0].set_xlabel('Tempo (s)')
axs[0].set_title('Sinal original')
axs[0].margins(0, 0.1)

axs[1].axvspan(0.04, 0.06, facecolor='r', alpha=0.15)
# axs[1].axvspan((0.04 - (pitch_period/samplerate)), 0.04, facecolor='g', alpha=0.2)
axs[1].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
axs[1].plot(time_axis, demo_signal, 'b', label='Sinal com picote')
axs[1].set_xticks(major_ticks)
axs[1].set_xticks(minor_ticks, minor=True)
axs[1].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
axs[1].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
axs[1].set_ylabel('Amplitude')
axs[1].set_xlabel('Tempo (s)')
axs[1].set_title('Sinal com picote')
axs[1].margins(0, 0.1)

axs[2].axvspan(0.04, 0.06, facecolor='r', alpha=0.15)
# axs[2].axvspan((0.04 - (pitch_period/samplerate)), 0.04, facecolor='g', alpha=0.2)
axs[2].axhline(0, color='r', linewidth=1, linestyle='--', alpha=0.5)
axs[2].plot(time_axis, corrected_signal, 'b', label='Sinal corrigido')
axs[2].set_xticks(major_ticks)
axs[2].set_xticks(minor_ticks, minor=True)
axs[2].grid(color='r', axis='x', linestyle='--', which='minor', linewidth=0.5, alpha=0.2)
axs[2].grid(color='r', axis='x', linestyle='--', which='major', linewidth=1, alpha=0.5)
axs[2].set_ylabel('Amplitude')
axs[2].set_xlabel('Tempo (s)')
axs[2].set_title('Sinal corrigido')
axs[2].margins(0, 0.1)

# Gráficos de FFT dos sinais
# T = 1.0 / samplerate
# N = len(original_signal)
# xf = np.linspace(0.0, 1.0 / (2.0 * T), N // 2)
#
# yf_original = fftpack.fft(original_signal)
# axs[0, 1].plot(xf, 2.0 / N * np.abs(yf_original[:N // 2]), label='FFT')
# axs[0, 1].set_ylabel('FFT')
# axs[0, 1].set_xlabel('Frequência (Hz)')
# axs[0, 1].set_title('Sinal original - FFT')
# axs[0, 1].margins(0, 0.1)
#
# yf_corrected = fftpack.fft(corrected_signal)
# axs[1, 1].plot(xf, 2.0 / N * np.abs(yf_corrected[:N // 2]), label='FFT')
# axs[1, 1].set_ylabel('FFT')
# axs[1, 1].set_xlabel('Frequência (Hz)')
# axs[1, 1].set_title('Sinal corrigido - FFT')
# axs[1, 1].margins(0, 0.1)

plt.tight_layout()
plt.show(block=True)
