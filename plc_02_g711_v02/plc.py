import numpy as np
import scipy.signal

from typing import Tuple

from audio_manipulation.read_audiofile_as_frames import ms_to_sample


def plc_algorithm_3_donotuse(frame_minus_1: np.ndarray,
                    frame_minus_2: np.ndarray,
                    frame_minus_3: np.ndarray,
                    frame_plus_1: np.ndarray,
                    samplerate: int) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:

    pitch_buffer = np.append(frame_minus_3[ms_to_sample(5, samplerate):], frame_minus_2)
    pitch_buffer = np.append(pitch_buffer, frame_minus_1)

    correlation_window_1 = pitch_buffer[-ms_to_sample(40, samplerate):]
    correlation_window_2 = pitch_buffer[-ms_to_sample(35, samplerate):-ms_to_sample(5, samplerate)]

    # Pitch detection
    max_abs_window_1 = abs(max(correlation_window_1, key=abs))
    if max_abs_window_1 != 0:
        normalized_window_1 = correlation_window_1 / max_abs_window_1
    else:
        normalized_window_1 = correlation_window_1

    max_abs_window_2 = abs(max(correlation_window_2, key=abs))
    if max_abs_window_2 != 0:
        normalized_window_2 = correlation_window_2 / max_abs_window_2
    else:
        normalized_window_2 = correlation_window_2

    normalized_cross_correlation = scipy.signal.correlate(normalized_window_1, normalized_window_2, method='fft')
    max_normalized_cross_correlation_index = np.argmax(normalized_cross_correlation)

    print("max_normalized_cross_correlation_index: {}".format(max_normalized_cross_correlation_index))

    min_pitch_freq = 66
    max_pitch_freq = 200
    pitch_period_difference = ms_to_sample(min_pitch_freq, samplerate) - ms_to_sample(max_pitch_freq, samplerate)
    best_corr_value = 0
    best_corr_index = 0
    for i in range(pitch_period_difference):
        pass




    pitch_period_samples = len(normalized_cross_correlation) - max_normalized_cross_correlation_index
    quarter_pitch_period_samples = int(pitch_period_samples/4)
    hanning_window = np.hanning(2*quarter_pitch_period_samples)
    hanning_window = hanning_window[:int(len(hanning_window)/2)]

    # pitch_period_frame = np.append(frame_minus_2, frame_minus_1)[len(frame_minus_1)+len(frame_minus_2)-pitch_period_samples:]
    pitch_period_frame = pitch_buffer[-pitch_period_samples:]

    # Modifies tail of last good frame, to ensure smooth transition to synthetic generated signal
    tail_of_frame_minus_1 = frame_minus_1[len(frame_minus_1)-quarter_pitch_period_samples:]
    modified_tail_of_frame_minus_1 = np.asarray([])
    for i in range(len(tail_of_frame_minus_1)):
        modified_sample_tail = tail_of_frame_minus_1[i]*hanning_window[-i-1]
        modified_sample_pitch_frame = pitch_period_frame[i]*hanning_window[i]
        modified_sample_synthetic_signal = modified_sample_tail + modified_sample_pitch_frame
        modified_tail_of_frame_minus_1 = np.append(modified_tail_of_frame_minus_1, modified_sample_synthetic_signal)
    frame_minus_1[len(frame_minus_1) - quarter_pitch_period_samples:] = modified_tail_of_frame_minus_1

    # Construction of synthetic signal
    synthetic_signal = np.zeros(len(frame_minus_1))
    index_pitch_period_frame = 0
    for i in range(len(synthetic_signal)):
        synthetic_signal[i] = pitch_period_frame[index_pitch_period_frame]
        index_pitch_period_frame = index_pitch_period_frame + 1
        if index_pitch_period_frame >= len(pitch_period_frame):
            index_pitch_period_frame = 0

    # Modifies head of following frame (for now it's always considered a good frame, gonna change in v02)
    for i in range(quarter_pitch_period_samples):
        modified_sample_head = frame_plus_1[i]*hanning_window[i]
        modified_sample_pitch_frame = pitch_period_frame[index_pitch_period_frame]*hanning_window[-i-1]
        index_pitch_period_frame = index_pitch_period_frame + 1
        if index_pitch_period_frame >= len(pitch_period_frame):
            index_pitch_period_frame = 0
        frame_plus_1[i] = modified_sample_head + modified_sample_pitch_frame

    return frame_minus_1, synthetic_signal, frame_plus_1

#######################################################
#######################################################


def plc_algorithm_2(frame_minus_1: np.ndarray,
                    frame_minus_2: np.ndarray,
                    frame_minus_3: np.ndarray,
                    frame_plus_1: np.ndarray,
                    samplerate: int) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:

    pitch_buffer = np.append(frame_minus_3[ms_to_sample(5, samplerate):], frame_minus_2)
    pitch_buffer = np.append(pitch_buffer, frame_minus_1)

    correlation_window_1 = pitch_buffer[-ms_to_sample(40, samplerate):]
    correlation_window_2 = pitch_buffer[-ms_to_sample(35, samplerate):-ms_to_sample(5, samplerate)]

    # Pitch detection
    max_abs_window_1 = abs(max(correlation_window_1, key=abs))
    if max_abs_window_1 != 0:
        normalized_window_1 = correlation_window_1 / max_abs_window_1
    else:
        normalized_window_1 = correlation_window_1

    max_abs_window_2 = abs(max(correlation_window_2, key=abs))
    if max_abs_window_2 != 0:
        normalized_window_2 = correlation_window_2 / max_abs_window_2
    else:
        normalized_window_2 = correlation_window_2

    normalized_cross_correlation = scipy.signal.correlate(normalized_window_1, normalized_window_2, method='fft')
    max_normalized_cross_correlation_index = np.argmax(normalized_cross_correlation)

    print("max_normalized_cross_correlation_index: {}".format(max_normalized_cross_correlation_index))

    pitch_period_samples = len(normalized_cross_correlation) - max_normalized_cross_correlation_index
    quarter_pitch_period_samples = int(pitch_period_samples/4)
    hanning_window = np.hanning(2*quarter_pitch_period_samples)
    hanning_window = hanning_window[:int(len(hanning_window)/2)]

    # pitch_period_frame = np.append(frame_minus_2, frame_minus_1)[len(frame_minus_1)+len(frame_minus_2)-pitch_period_samples:]
    pitch_period_frame = pitch_buffer[-pitch_period_samples:]

    # Modifies tail of last good frame, to ensure smooth transition to synthetic generated signal
    tail_of_frame_minus_1 = frame_minus_1[len(frame_minus_1)-quarter_pitch_period_samples:]
    modified_tail_of_frame_minus_1 = np.asarray([])
    for i in range(len(tail_of_frame_minus_1)):
        modified_sample_tail = tail_of_frame_minus_1[i]*hanning_window[-i-1]
        modified_sample_pitch_frame = pitch_period_frame[i]*hanning_window[i]
        modified_sample_synthetic_signal = modified_sample_tail + modified_sample_pitch_frame
        modified_tail_of_frame_minus_1 = np.append(modified_tail_of_frame_minus_1, modified_sample_synthetic_signal)
    frame_minus_1[len(frame_minus_1) - quarter_pitch_period_samples:] = modified_tail_of_frame_minus_1

    # Construction of synthetic signal
    synthetic_signal = np.zeros(len(frame_minus_1))
    index_pitch_period_frame = 0
    for i in range(len(synthetic_signal)):
        synthetic_signal[i] = pitch_period_frame[index_pitch_period_frame]
        index_pitch_period_frame = index_pitch_period_frame + 1
        if index_pitch_period_frame >= len(pitch_period_frame):
            index_pitch_period_frame = 0

    # Modifies head of following frame (for now it's always considered a good frame, gonna change in v02)
    for i in range(quarter_pitch_period_samples):
        modified_sample_head = frame_plus_1[i]*hanning_window[i]
        modified_sample_pitch_frame = pitch_period_frame[index_pitch_period_frame]*hanning_window[-i-1]
        index_pitch_period_frame = index_pitch_period_frame + 1
        if index_pitch_period_frame >= len(pitch_period_frame):
            index_pitch_period_frame = 0
        frame_plus_1[i] = modified_sample_head + modified_sample_pitch_frame

    return frame_minus_1, synthetic_signal, frame_plus_1





#######################################################
#######################################################


def plc_algorithm(frame_minus_1: np.ndarray, frame_minus_2: np.ndarray, frame_plus_1: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:

    # Pitch detection
    max_abs_signal1 = abs(max(frame_minus_1, key=abs))
    if max_abs_signal1 != 0:
        normalized_signal1 = frame_minus_1 / max_abs_signal1
    else:
        normalized_signal1 = frame_minus_1
    max_abs_signal2 = abs(max(frame_minus_2, key=abs))
    if max_abs_signal2 != 0:
        normalized_signal2 = frame_minus_2 / max_abs_signal2
    else:
        normalized_signal2 = frame_minus_2

    normalized_cross_correlation = scipy.signal.correlate(normalized_signal1, normalized_signal2, method='fft')
    max_normalized_cross_correlation_index = np.argmax(normalized_cross_correlation)

    pitch_period_samples = len(normalized_cross_correlation) - max_normalized_cross_correlation_index
    quarter_pitch_period_samples = int(pitch_period_samples/4)
    hanning_window = np.hanning(2*quarter_pitch_period_samples)
    hanning_window = hanning_window[:int(len(hanning_window)/2)]

    pitch_period_frame = np.append(frame_minus_2, frame_minus_1)[len(frame_minus_1)+len(frame_minus_2)-pitch_period_samples:]

    # Modifies tail of last good frame, to ensure smooth transition to synthetic generated signal
    tail_of_frame_minus_1 = frame_minus_1[len(frame_minus_1)-quarter_pitch_period_samples:]
    modified_tail_of_frame_minus_1 = np.asarray([])
    for i in range(len(tail_of_frame_minus_1)):
        modified_sample_tail = tail_of_frame_minus_1[i]*hanning_window[-i-1]
        modified_sample_pitch_frame = pitch_period_frame[i]*hanning_window[i]
        modified_sample_synthetic_signal = modified_sample_tail + modified_sample_pitch_frame
        modified_tail_of_frame_minus_1 = np.append(modified_tail_of_frame_minus_1, modified_sample_synthetic_signal)
    frame_minus_1[len(frame_minus_1) - quarter_pitch_period_samples:] = modified_tail_of_frame_minus_1

    # Construction of synthetic signal
    synthetic_signal = np.zeros(len(frame_minus_1))
    index_pitch_period_frame = 0
    for i in range(len(synthetic_signal)):
        synthetic_signal[i] = pitch_period_frame[index_pitch_period_frame]
        index_pitch_period_frame = index_pitch_period_frame + 1
        if index_pitch_period_frame >= len(pitch_period_frame):
            index_pitch_period_frame = 0

    # Modifies head of following frame (for now it's always considered a good frame, gonna change in v02)
    for i in range(quarter_pitch_period_samples):
        modified_sample_head = frame_plus_1[i]*hanning_window[i]
        modified_sample_pitch_frame = pitch_period_frame[index_pitch_period_frame]*hanning_window[-i-1]
        index_pitch_period_frame = index_pitch_period_frame + 1
        if index_pitch_period_frame >= len(pitch_period_frame):
            index_pitch_period_frame = 0
        frame_plus_1[i] = modified_sample_head + modified_sample_pitch_frame

    return frame_minus_1, synthetic_signal, frame_plus_1
