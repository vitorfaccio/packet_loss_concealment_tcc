import os

from plc import *
from audio_manipulation.read_audiofile_as_frames import *

CORRECTED_SET_DIR = Path(os.getcwd()).parent.resolve() / 'TCD-VOIP_for_use' / 'corrected_g711_v2'

# ARQUIVO NÃO UTILIZADO.
# Algoritmo baseado no Anexo A do codec G.711
# é implementado na pasta "plc_01_g711_v01".

if __name__ == '__main__':

    # Variables setup
    # G.711 uses frames of 10ms
    frame_length_ms = 20
    samplerate = 16000  # Expected samplerate for given files

    try:
        # Iterates over wav file paths in './audios/' folder:
        all_audio_paths = sorted(Path(os.getcwd()).parent.resolve().glob('TCD-VOIP_for_use/chop_20ms_160ms_16khz/*.wav'))
        for audio_path in all_audio_paths:

            # Reads audio file as list of frames:
            frames, sr_fromfile = read_audiofile_as_frames(audio_path, frame_length_ms)
            if sr_fromfile != samplerate:
                raise RuntimeError("Samplerate in file {} is wrong, identified value: {}".format(audio_path.name, sr_fromfile))
            frame_length_samples = ms_to_sample(frame_length_ms, samplerate)

            # Run audio frames
            for frame_index in range(2, len(frames)-1):
                # Detects if frame is valid or not:
                if not get_frame_validity(frames[frame_index]):
                    # TODO: Different processing for first bad frame or following ones

                    # Runs PLC algorithm over problematic frame:
                    frames[frame_index-1], frames[frame_index], frames[frame_index+1] = plc_algorithm_2(
                        frames[frame_index-1], frames[frame_index-2], frames[frame_index-3], frames[frame_index+1], samplerate)

                # Store frame

            # Save file
            print("Starting file save")
            new_file_name = audio_path.name
            new_file_name = 'CORRECTED_' + new_file_name
            frames_to_save = np.reshape(frames, (1, frame_length_samples*len(frames)))[0]
            frames_to_save = frames_to_save.astype('int16')
            sf.write(CORRECTED_SET_DIR / new_file_name, frames_to_save, samplerate)

    except Exception as e:
        print("Error occurred: {}".format(e))
