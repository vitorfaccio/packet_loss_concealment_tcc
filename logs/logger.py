import logging
from pathlib import Path
from logging.handlers import RotatingFileHandler


def configure_logger(working_dir: Path, execution_date: str) -> logging.Logger:
    logger = logging.getLogger("Project Logger")
    formatter = logging.Formatter("%(asctime)s.%(msecs)03d %(message)s", "%d/%m/%Y %H:%M:%S")

    error_handler = logging.FileHandler(working_dir / 'error_messages' / '{}.log'.format(execution_date))
    error_handler.setLevel(logging.ERROR)
    logger.addHandler(error_handler)
    error_handler.setFormatter(formatter)

    debug_handler = RotatingFileHandler(working_dir / 'debug_messages' / '{}.log'.format(execution_date), maxBytes=100000, backupCount=1)
    debug_handler.setLevel(logging.DEBUG)
    logger.addHandler(debug_handler)
    debug_handler.setFormatter(formatter)

    logger.setLevel(logging.DEBUG)

    return logger
