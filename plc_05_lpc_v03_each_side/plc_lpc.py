import librosa
import numpy as np
import scipy.signal
from matplotlib import pyplot as plt

from audio_manipulation.read_audiofile_as_frames import *


def pitch_detector_gunduzhan(e_n: np.ndarray, p_min: int, p_max: int, C: int) -> Tuple[int, int]:
    # p_min and p_max: minimum and maximum pitch periods in samples
    # C: Correlation size

    max_pitch = 0
    max_pitch_index = 0
    for i in range(p_min, p_max+1):
        sum_num = 0.
        sum_den = 0.
        for n in range(len(e_n)-C, len(e_n)):
            sum_num = sum_num + (e_n[n-i] * e_n[n])
            sum_den = sum_den + (e_n[n-i] * e_n[n-i])
        pitch = sum_num / np.sqrt(sum_den) if sum_den != 0. else 0.

        if pitch > max_pitch:
            max_pitch = pitch
            max_pitch_index = i

    # TODO: Gunduzhan, p.781: "Using a multiple of the pitch period ( 10 ms) for signals with
    #   low autocorrelation, we do not introduce artificial frequencies
    #   that could be caused by repeating a small segment many times.
    #   On the other hand, if the calculated autocorrelation is low but
    #   the previous speech happens to be voiced, we still maintain periodicity and generate a smooth excitation."
    if max_pitch_index < p_min:
        max_pitch_index = p_min
    return max_pitch_index, max_pitch


def plc_algorithm_lpc_forward(frame_minus_one: np.ndarray) -> np.ndarray:
    G = 0.01
    alpha = 0.7
    beta = 0.3
    lpc_order = 50
    lpc_fwd_samples_window = np.flip(frame_minus_one)

    lpc_fwd_coefficients = librosa.lpc(lpc_fwd_samples_window, order=lpc_order)  # LPC analysis length: 1 frame
    predicted_frame_fwd = np.zeros(len(frame_minus_one))

    pitch_max = 238  # 68 Hz
    pitch_min = 40   # 400 Hz
    pitch_correlation_size = 200
    pitch_period, pitch_value = pitch_detector_gunduzhan(frame_minus_one, pitch_min, pitch_max, pitch_correlation_size)
    pwr_frame = frame_minus_one[-pitch_period:]

    sum_value = 0
    for i in range(1, lpc_order):
        sum_value = lpc_fwd_coefficients[i] * frame_minus_one[-i-1]
    predicted_frame_fwd[0] = (sum_value + pwr_frame[0]*G)*alpha*(-1) + pwr_frame[0]*beta

    pwr_frame_index = 1
    for k in range(1, len(predicted_frame_fwd)):
        sum_value = 0.
        for j in range(1, lpc_order):
            if j <= k:
                single_value = lpc_fwd_coefficients[j] * predicted_frame_fwd[k-j]
            else:
                single_value = lpc_fwd_coefficients[j] * frame_minus_one[k-j-1]
            sum_value += single_value
        predicted_frame_fwd[k] = (sum_value + pwr_frame[pwr_frame_index]*G)*alpha*(-1) + pwr_frame[pwr_frame_index]*beta
        if pwr_frame_index >= len(pwr_frame)-1:
            pwr_frame_index = 0
        else:
            pwr_frame_index = pwr_frame_index + 1
    return predicted_frame_fwd


def plc_algorithm_lpc_backward(frame_plus_one: np.ndarray) -> np.ndarray:
    G = 0.01
    alpha = 0.7
    beta = 0.3
    lpc_order = 50
    lpc_bwd_samples_window = frame_plus_one

    lpc_fwd_coefficients = librosa.lpc(lpc_bwd_samples_window, order=lpc_order)  # LPC analysis length: 1 frame
    predicted_frame_bwd = np.zeros(len(frame_plus_one))

    pitch_max = 238  # 68 Hz
    pitch_min = 40   # 400 Hz
    pitch_correlation_size = 200
    pitch_period, pitch_value = pitch_detector_gunduzhan(frame_plus_one, pitch_min, pitch_max, pitch_correlation_size)
    pwr_frame = frame_plus_one[:pitch_period]

    sum_value = 0.
    for i in range(1, lpc_order):  # TODO: ver esse 1 do range ou o -1 do frame_plus_one[i-1]
        sum_value = lpc_fwd_coefficients[i] * frame_plus_one[i-1]
    predicted_frame_bwd[-1] = (sum_value + pwr_frame[-1]*G)*alpha*(-1) + pwr_frame[-1]*beta

    pwr_frame_index = -2
    for k in range(1, len(predicted_frame_bwd)):
        sum_value = 0.
        for j in range(1, lpc_order):
            if j <= k:
                sum_value += lpc_fwd_coefficients[j] * predicted_frame_bwd[-k-1+j]
            else:
                sum_value += lpc_fwd_coefficients[j] * frame_plus_one[-k-1+j]
        final_value = (sum_value + pwr_frame[pwr_frame_index]*G)*alpha*(-1) + pwr_frame[pwr_frame_index]*beta
        predicted_frame_bwd[-k-1] = final_value
        if pwr_frame_index <= -len(pwr_frame):
            pwr_frame_index = 0
        else:
            pwr_frame_index = pwr_frame_index - 1
    return predicted_frame_bwd


