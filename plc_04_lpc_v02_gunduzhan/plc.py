import librosa
import numpy as np
import scipy.signal
from matplotlib import pyplot as plt

from audio_manipulation.read_audiofile_as_frames import *


def get_pitch_period_signal(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray) -> np.ndarray:
    # Pitch detection
    max_abs_signal1 = abs(max(frame_minus_one, key=abs))
    if max_abs_signal1 != 0:
        normalized_signal1 = frame_minus_one / max_abs_signal1
    else:
        normalized_signal1 = frame_minus_one
    max_abs_signal2 = abs(max(frame_minus_two, key=abs))
    if max_abs_signal2 != 0:
        normalized_signal2 = frame_minus_two / max_abs_signal2
    else:
        normalized_signal2 = frame_minus_two

    normalized_cross_correlation = scipy.signal.correlate(normalized_signal1, normalized_signal2, method='fft')
    max_normalized_cross_correlation_index = np.argmax(normalized_cross_correlation)
    pitch_period_samples = len(normalized_cross_correlation) - max_normalized_cross_correlation_index
    pitch_period_signal = np.append(frame_minus_two, frame_minus_one)[-pitch_period_samples:]

    return pitch_period_signal


def plc_algorithm_lpc_elsabrouty_eq4(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray, samplerate: int) -> np.ndarray:
    G = 0.01
    alpha = 0.7
    beta = 0.3
    lpc_order = 50
    lpc_analysis_window_length = ms_to_sample(20, samplerate)
    lpc_fwd_samples_window = np.flip(frame_minus_one[-lpc_analysis_window_length:])

    lpc_fwd_coefficients = librosa.lpc(lpc_fwd_samples_window, order=lpc_order)  # LPC analysis length: 1 frame
    predicted_frame_fwd = np.zeros(len(frame_minus_one))

    pwr_frame = get_pitch_period_signal(frame_minus_two, frame_minus_one)
    predicted_value = 0
    for i in range(1, lpc_order):
        single_value = lpc_fwd_coefficients[i] * frame_minus_one[-i-1]
        predicted_value += single_value
    predicted_frame_fwd[0] = (predicted_value + pwr_frame[0]*G)*alpha*(-1) + pwr_frame[0]*beta

    pwr_frame_index = 1
    for k in range(1, len(predicted_frame_fwd)):
        predicted_value = 0
        for j in range(1, lpc_order):
            if j <= k:
                single_value = lpc_fwd_coefficients[j] * predicted_frame_fwd[k-j]
            else:
                single_value = lpc_fwd_coefficients[j] * frame_minus_one[k-j-1]
            predicted_value += single_value
        predicted_frame_fwd[k] = (predicted_value + pwr_frame[pwr_frame_index]*G)*alpha*(-1) + pwr_frame[pwr_frame_index]*beta
        if pwr_frame_index >= len(pwr_frame)-1:
            pwr_frame_index = 0
        else:
            pwr_frame_index = pwr_frame_index + 1
    return predicted_frame_fwd


#########################################################################


def pitch_detector_gunduzhan(e_n: np.ndarray, p_min: int, p_max: int, C: int) -> Tuple[int, int]:
    # p_min and p_max: minimum and maximum pitch periods in samples
    # C: Correlation size

    max_pitch = 0
    max_pitch_index = 0
    for i in range(p_min, p_max+1):
        sum_num = 0.
        sum_den = 0.
        for n in range(len(e_n)-C, len(e_n)):
            sum_num = sum_num + (e_n[n-i] * e_n[n])
            sum_den = sum_den + (e_n[n-i] * e_n[n-i])
        pitch = sum_num / np.sqrt(sum_den)

        if pitch > max_pitch:
            max_pitch = pitch
            max_pitch_index = i

    return max_pitch_index, max_pitch


def plc_algorithm_lpc_gunduzhan(frame_minus_two: np.ndarray, frame_minus_one: np.ndarray, samplerate: int) -> np.ndarray:
    # Parameters
    G = 0.01
    alpha = 0.7
    beta = 0.3

    # Delay
    D = 80  # OLA window, or delay, Gunduzhan use 40 for 8kHz (80 = 1/4 of 20ms frame)

    # History signal
    h_n = np.append(frame_minus_two, frame_minus_one)
    B = len(h_n)
    N = len(frame_minus_one)

    # LP filter
    L = 50  # LP filter order
    lpc_samples_window = np.flip(h_n)
    lpc_coefficients = librosa.lpc(lpc_samples_window, order=L)  # LPC analysis length: 1 frame

    # Residual signal e(n)
    e_n = np.zeros(B-L)
    for n in range(B-L):
        sum_value = 0.
        for i in range(L):
            sum_value = sum_value + (lpc_coefficients[i] * h_n[n+L-i])
        e_n[n] = h_n[n+L] - sum_value

    # Pitch period
    pitch_max = 238  # 68 Hz
    pitch_min = 40   # 400 Hz
    pitch_correlation_size = 200
    pitch_period, pitch_value = pitch_detector_gunduzhan(e_n, pitch_min, pitch_max, pitch_correlation_size)

    # pitch_value_threshold = 10
    # if pitch_value < pitch_value_threshold:
    #     pass
    # TODO: Adicionar checagem de pitch value, provavelmente dentro da função de detecção,
    #  para colocar um múltiplo maior que 10ms em caso de pitch value muito pequeno (tonal artifacts)

    # Excitation signal e_hat_n
    e_hat_n = np.zeros(N + 2*D)
    pitch_period_of_e_n = e_n[-pitch_period:]

    e_hat_n[:D] = e_n[-D-pitch_period:-pitch_period]
    index_pitch_period_frame = 0
    for n in range(len(e_hat_n)-D):
        e_hat_n[n+D] = pitch_period_of_e_n[index_pitch_period_frame]
        index_pitch_period_frame = index_pitch_period_frame + 1
        if index_pitch_period_frame >= len(pitch_period_of_e_n):
            index_pitch_period_frame = 0

    # Synthesis filter
    x_hat_n = np.zeros(N + 2*D)
    x_hat_n_initial_condition = h_n[-D-L:]
    for n in range(len(x_hat_n)):
        sum_value_2 = 0.
        for j in range(L):
            if j >= n:
                sum_value_2 = sum_value_2 + (lpc_coefficients[j] * x_hat_n_initial_condition[n - j])  # Initial conditions...
            else:
                sum_value_2 = sum_value_2 + (lpc_coefficients[j] * x_hat_n[n - j])  # x_hat_n...
        x_hat_n[n] = sum_value_2 + e_hat_n[n]

    output_signal = x_hat_n[D:-D]
    return output_signal
